package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Session;

import java.util.List;

public interface ISessionModelRepository extends IModelRepository<Session> {
    @Nullable
    List<Session> findAll();

    @Nullable
    List<Session> findAllByUser(@Nullable String userId);

    @Nullable
    Session findOneById(@NotNull String id);

    @Nullable
    Session findOneByIdByUser(@Nullable String userId, @Nullable String id);

    void clear();

    void removeAll(@Nullable String userId);

    void remove(@Nullable String userId, @NotNull Session model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    long getSize();

    int getSizeByUser(@Nullable String userId);

    int existsById(@Nullable String userId, @Nullable String id);
}
