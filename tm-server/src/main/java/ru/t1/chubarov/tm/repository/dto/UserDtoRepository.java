package ru.t1.chubarov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chubarov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM UserDTO p ", UserDTO.class)
                .setFirstResult(1)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM UserDTO p WHERE p.id = :id", UserDTO.class)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO").executeUpdate();
    }

    @Override
    public void remove(@NotNull UserDTO model) {
        entityManager
                .createQuery("DELETE FROM UserDTO p WHERE p.id = :id")
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM UserDTO p", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

}
