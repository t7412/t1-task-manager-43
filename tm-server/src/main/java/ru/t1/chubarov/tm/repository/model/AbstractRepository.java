package ru.t1.chubarov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.IModelRepository;
import ru.t1.chubarov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IModelRepository<M> {

    @NotNull
    public final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Nullable
    @Override
    public abstract List<M> findAll();

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public abstract M findOneById(@NotNull final String id);

    @Override
    public abstract void clear();

    @Override
    public void remove(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public abstract long getSize();

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
