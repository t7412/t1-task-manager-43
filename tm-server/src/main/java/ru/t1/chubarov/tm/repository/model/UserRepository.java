package ru.t1.chubarov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserModelRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<User> findAll() {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM User p ", User.class)
                .setFirstResult(1)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM User p WHERE p.id = :id", User.class)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }
    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM User p WHERE p.login = :login", User.class)
                .setParameter("login", login)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }
    @Nullable
    @Override
    public User findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM User p WHERE p.email = :email", User.class)
                .setParameter("email", email)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM User").executeUpdate();
    }

    @Override
    public void remove(@NotNull User model) {
        entityManager
                .createQuery("DELETE FROM User p WHERE p.id = :id")
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM User p", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int isLoginExist(@NotNull String login) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM User p WHERE p.login = :login", Integer.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int isEmailExist(@NotNull String email) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM User p WHERE p.email = :email", Integer.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getSingleResult();
    }

}
