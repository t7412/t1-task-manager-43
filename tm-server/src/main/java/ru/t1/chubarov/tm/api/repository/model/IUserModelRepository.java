package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.User;

import java.util.List;

public interface IUserModelRepository extends IModelRepository<User> {

    @Nullable
    List<User> findAll();

    @Nullable
    User findOneById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String id);

    @Nullable
    User findByEmail(@NotNull String id);

    void clear();

    void remove(@NotNull User model);

    long getSize();

    int isLoginExist(@NotNull String login);

    int isEmailExist(@NotNull String mail);

}
