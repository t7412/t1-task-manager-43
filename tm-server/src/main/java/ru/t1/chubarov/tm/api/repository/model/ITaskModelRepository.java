package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskModelRepository extends IModelRepository<Task>{

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAllByUser(@Nullable String userId);

    @Nullable
    Task findOneById(@NotNull String id);

    @Nullable
    Task findOneByIdByUser(@Nullable String userId, @Nullable String id);

    void clear();

    void removeAll(@Nullable String userId);

    void remove(@Nullable String userId, @NotNull Task model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    long getSize();

    int getSizeByUser(@Nullable String userId);

    int existsById(@Nullable String userId, @Nullable String id);

}
