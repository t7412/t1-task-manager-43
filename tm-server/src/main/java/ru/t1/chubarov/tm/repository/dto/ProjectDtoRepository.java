package ru.t1.chubarov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectDtoRepository extends AbstractUserOwnerDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager
                .createQuery("SELECT p.id, p.name, p.userId, p.status, p.description, p.created FROM ProjectDTO p ", ProjectDTO.class)
                .setFirstResult(1)
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT p.id, p.name, p.userId, p.status, p.description, p.created FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(1)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p.id, p.name, p.userId, p.status, p.description, p.created FROM ProjectDTO p WHERE p.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIdByUser(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT p.id, p.name, p.userId, p.status, p.description, p.created FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }


    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO p WHERE p.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void remove(@Nullable String userId, @NotNull ProjectDTO model) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO p WHERE p.userId = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO p WHERE p.userId = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM ProjectDTO p", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int getSizeByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int existsById(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

}
