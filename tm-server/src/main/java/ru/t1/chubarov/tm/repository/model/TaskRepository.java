package ru.t1.chubarov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.ITaskModelRepository;
import ru.t1.chubarov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskModelRepository {


    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Task>  findAll() {
        return entityManager
                .createQuery("SELECT p.id,p.name,p.created,p.description,p.project.id,p.status,p.user.id FROM Task p ", Task.class)
                .setFirstResult(1)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT p.id,p.name,p.created,p.description,p.project.id,p.status,p.user.id FROM Task p WHERE p.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .setFirstResult(1)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p.id,p.name,p.created,p.description,p.project.id,p.status,p.user.id FROM Task p WHERE p.id = :id", Task.class)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findOneByIdByUser(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT p.id,p.name,p.created,p.description,p.project.id,p.status,p.user.id FROM Task p WHERE p.user.id = :userId AND p.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        entityManager
                .createQuery("DELETE FROM Task p WHERE p.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void remove(@Nullable String userId, @NotNull Task model) {
        entityManager
                .createQuery("DELETE FROM Task p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM Task p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Task p", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int getSizeByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Task p WHERE p.user.id = :userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int existsById(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Task p WHERE p.user.id = :userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

}
