package ru.t1.chubarov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

}
